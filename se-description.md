# SE Series Flowchart Description & Conventions, DOC N: G1

___

## a. General Description

Following document describes purpose and represent basic workflow rules

This workflow is designed for companies that have no any automated quality assurance
department, also have no any unit, integration and acceptance test coded/written for using
in automated CI/CD processes. Also, this workflow need detailed decomposed and unalterable
backlog pool for sprint period, however it's required for any other workflows in this world also.

Requirements,

| Directive                       | Required |
|---------------------------------|----------|
| QA Automation                   | NO       |
| Auto Tests in CI/CD             | NO       |
| Frozen Sprint BackLog           | YES      |
| Detailed Decomposed Sprint      | YES      |
| High Skilled Specialists        | NO       |
| Need blindly follow conventions | YES      |

## b. Commit Messages

For easy forming and standardisation purposes, inside software engineer team need to be contracted following convention about formalization of commit messages.

For issue's , by meaning, that will mostly meet (Add: Change: Remove: Fix: Reformat: is a `{meaning}`)
general format is 
```
{meaning}: {what is done description}
```
1. `addition` message should start from `Add: {what is done description}`
2. `changes` message should start from `Change: {what is done description}`
3. `deletion/removes`, something, message should start from `Remove: {what is done description}`
4. `fixing`, something, message should start from `Fix: {what is done description}`
5. `reformating`, something, message should start from `Reformat: {what is done description}`

There is also varying formats of forming messages, for example for our format you could add
commands for automatic parent task status changes, for example
```
{task id} {command}
{meaning}: {what is done description}

or

{meaning}: {what is done description}
{task id} {command}
```

Real world final commit message example,

```
Add: create user address DTO and Service
#GP345698 Code Review

where first line is our {meaning}: {what is done description}

second line #GP345698 is {task id} and Code Review is {command}
```

***Notice*** `Command should be used only for final commit`

***Notice*** `Command syntax and variations depends on automation tools that is used inside team`

## c. The size of changes in one merge request, why it should be small

**IF** - *code written for one merge request and need's to be reviewed by other team members* - **:**

**Size of changes should be max 10-15 files**

**ELSE**

**In such size so that it could be possible to revert changes**

This rule is regulating code quality and modularity of committed code blocks,
also when is necessary to review code for much bigger chunks it's quite comfortable to view
changes per commit, if sizes are not regulated it's a marker for wrong workflow especially
neglection of task forming and decomposition, neglection of  immutability of backlog pool in a
current sprint and besides this it could be negligence of engineer which is trying to push in
tasks that not related to current subject.

It's allowed to do some `reformatting` commits, but it's recommended to do separately , by single
merge request. If you have to reformat something for happy coding, you have to do it before
beginning of implementation, because if you put in one merge request - reformatting and other
meanings, the reviewers and in future team members and even you, would face to difficulties to 
understand what's going on in that mess.


## d. The General Diagram 

![Diagram](images/process.drawio.svg)